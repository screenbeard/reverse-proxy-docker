FROM nginx
COPY ./nginx.conf /etc/nginx/nginx.conf
COPY ./dhparams.pem /etc/nginx/dhparams.pem
COPY ./ssl.conf /etc/nginx/ssl.conf
COPY ./common.conf /etc/nginx/common.conf
COPY ./common_location.conf /etc/nginx/common_location.conf
RUN groupadd docker-share -g 1982 && \
    usermod -a -G docker-share nginx